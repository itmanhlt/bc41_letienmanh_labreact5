import { combineReducers } from "redux";
import { phoneReducer } from "./phoneReducer";

export const rootReducer = combineReducers({
  phoneReducer,
});
