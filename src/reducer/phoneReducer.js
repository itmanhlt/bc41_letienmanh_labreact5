import { dataPhone } from "../Phone_Redux/dataPhone";

let initialValue = {
  listPhone: dataPhone,
  detail: dataPhone[0],
};

export const phoneReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "CHANGE_DETAIL": {
      return { ...state, detail: action.payload };
    }
    default:
      return state;
  }
};
