import React, { Component } from "react";
import { connect } from "react-redux";

class ItemPhone extends Component {
  render() {
    let { hinhAnh, tenSP, giaBan } = this.props.data;
    return (
      <div className="col-4">
        <div style={{ height: "100%" }} className="card">
          <img
            style={{ height: "400px", objectFit: "cover" }}
            src={hinhAnh}
            className="card-img-top my-2"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">{tenSP}</h5>
            <p className="card-text">{giaBan.toLocaleString()} VNĐ</p>
            <button
              onClick={() => this.props.handleChangeDetail(this.props.data)}
              className="btn btn-primary"
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeDetail: (phone) => {
      dispatch({
        type: "CHANGE_DETAIL",
        payload: phone,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemPhone);
