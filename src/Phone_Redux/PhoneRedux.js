import React, { Component } from "react";
import ListPhone from "./ListPhone";
import PhoneDetail from "./PhoneDetail";

export default class PhoneRedux extends Component {
  render() {
    const containerStyle = {
      width: "90%",
      margin: "0 auto",
    };
    return (
      <div style={containerStyle}>
        <h2 className="text-center">PhoneRedux</h2>
        <ListPhone />
        <PhoneDetail />
      </div>
    );
  }
}
