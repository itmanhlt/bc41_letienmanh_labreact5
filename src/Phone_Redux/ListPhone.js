import React, { Component } from "react";
import { connect } from "react-redux";
import ItemPhone from "./ItemPhone";

class ListPhone extends Component {
  render() {
    return (
      <div>
        <h2 className="text-success">Danh sách sản phẩm</h2>
        <div className="row mt-0">
          {this.props.listPhone.map((item, index) => {
            return <ItemPhone key={index} data={item} />;
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { listPhone: state.phoneReducer.listPhone };
};

export default connect(mapStateToProps)(ListPhone);
