import "./App.css";
import PhoneRedux from "./Phone_Redux/PhoneRedux";

function App() {
  return (
    <div>
      <PhoneRedux />
    </div>
  );
}

export default App;
